﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class start : MonoBehaviour {

    public GameObject LoadingObj;
    public GameObject StartObj;
    public Slider slider;

    public void OnStartButton()
    {

        LoadingObj.SetActive(true);

        StartCoroutine(LoadData());
    }

    IEnumerator LoadData()
    {
        AsyncOperation Async = SceneManager.LoadSceneAsync("main", LoadSceneMode.Single);

        while (!Async.isDone)
        {
            float prograssVal = Mathf.Clamp01(Async.progress / 0.9f);
            slider.value = prograssVal;
            yield return null;
        }
    }


   public void OnExitButton()
    {

    }
}
