﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemy : MonoBehaviour
{
    //型宣言
    float currentTime;
    float mobSpeed;
    bool RightSide;
    public Player player;
    
    GameObject[] Dropitem = new GameObject[13];

    public Vector3 currentPos;
    public bool isDropItem;


    // Use this for initialization
    void Start()
    {
        player = GameObject.FindWithTag("Player").GetComponent<Player>();
        Dropitem[0] = (GameObject)Resources.Load("Prefab/item1");
        Dropitem[1] = (GameObject)Resources.Load("Prefab/item2");
        Dropitem[2] = (GameObject)Resources.Load("Prefab/item3");
        Dropitem[3] = (GameObject)Resources.Load("Prefab/item4");
        Dropitem[4] = (GameObject)Resources.Load("Prefab/item5");
        Dropitem[5] = (GameObject)Resources.Load("Prefab/snakeItem1");
        Dropitem[6] = (GameObject)Resources.Load("Prefab/snakeItem2");
        Dropitem[7] = (GameObject)Resources.Load("Prefab/snakeItem3");
        Dropitem[8] = (GameObject)Resources.Load("Prefab/snakeItem4");
        Dropitem[9] = (GameObject)Resources.Load("Prefab/SpeedupItem1");
        Dropitem[10] = (GameObject)Resources.Load("Prefab/SpeedupItem2");
        Dropitem[11] = (GameObject)Resources.Load("Prefab/SpeedupItem3");
        Dropitem[12] = (GameObject)Resources.Load("Prefab/SpeedupItem4");

        currentTime = 0.0f;
        isDropItem = false;
        mobSpeed = Random.Range(0.5f, 5.0f);
    }


    //オブジェクトが生成されたとき
    private void OnEnable()
    {
        //生成位置によって右に行くか左に行くかを決める
        if (transform.position.x == 11)
        {
            RightSide = true;
        }
        else
        {
            RightSide = false;
        }
    }

    // 毎フレーム呼び出し
    void Update()
    {
        if (player.isPlaying)
        { 
        //アイテムを落としているかの判定
        if (isDropItem == true)
        {

            //アイテムドロップ時、移動停止
            currentPos = transform.position;
            transform.position = currentPos;
            StartCoroutine(MovePos());
        }
        else
        {
            //敵キャラの移動
            if (RightSide == true)
            {
                transform.position -= Vector3.right * Time.deltaTime * mobSpeed;
            }
            else
            {
                transform.position += Vector3.right * Time.deltaTime * mobSpeed;
            }
        }
        //2.5秒ごとで落下
        currentTime += Time.deltaTime;
            if (2.5f < currentTime)
            {
                currentTime = 0;
                int DropItemNum = -1;
                int[] beforeDropItem = new int[2];
                //敵の種類の判定
                if (gameObject.tag == "nomalEnemy")
                {
                    DropItemNum = Random.Range(0, 4);
                }
                else if (gameObject.tag == "garbageEnemy")
                {
                    DropItemNum = 4;
                }
                else if (gameObject.tag == "SnakeEnemy")
                {
                    if (DropItemNum == -1)
                    {
                        DropItemNum = Random.Range(5, 9);
                    }
                    else if (DropItemNum > 10)
                    {
                        DropItemNum = 5;
                    }
                    else
                    {
                        DropItemNum++;
                    }

                }
                else if (gameObject.tag == "SpeedupEnemy")
                {
                    if (DropItemNum == -1)
                    {
                        DropItemNum = Random.Range(9, 13);
                    }
                    else if (DropItemNum > 14)
                    {
                        DropItemNum = 5;
                    }
                    else
                    {
                        DropItemNum++;
                    }
                }

                //ゴミおばさん以外はアイテムの落下時止まるように
                if (gameObject.tag != "garbageEnemy")
                {
                    isDropItem = true;
                }
                //アイテムの落下
                Instantiate(Dropitem[DropItemNum], new Vector3(transform.position.x, transform.position.y - 0.5f, 3), new Quaternion());
            }
        }

        //範囲外に行ったら殺す
        if (transform.position.x > 12)
        {
            Destroy(gameObject);
        }
        else if (transform.position.x < -12)
        {
            Destroy(gameObject);
        }
    }
    
    IEnumerator MovePos()
    {
        yield return new WaitForSeconds(1.0f);
        isDropItem = false;
    }

}