﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UI : MonoBehaviour
{

    //変数宣言
    float currentTime;
    float times;
    int highScore;
    public Score score;
    public Player player;
    public Audio audioScript;

    [Header("mainCanvas gameObject")]
        [SerializeField]
            GameObject gameUI;
        [SerializeField]
            Image TimeGauge;
        [SerializeField]
            Text[] Scoretext = new Text[5];
        [SerializeField]
            GameObject ReadyText;
        [SerializeField]
            GameObject GoText;

    [Header("Result gameObject")]
        [SerializeField]
            Text GameTime;
        [SerializeField]
            Text ResultScore;
        [SerializeField]
            GameObject result;
        [SerializeField]
            GameObject TimeUp;

    [Header("Pause gameOject")]
        [SerializeField]
            GameObject pause;
    
    
    // Use this for initialization
    void Start()
    {

        StartCoroutine(GameReady());
        StartCoroutine(ResultReady());
        //変数初期化
        TimeUp.SetActive(false);
        currentTime = 0;
        Time.timeScale = 1.0f;
        ReadyText.SetActive(true);
        GoText.SetActive(false);
        audioScript.StopAudio();
        audioScript.StartAudio(2);
        if (PlayerPrefs.HasKey("highScore"))
        {
            highScore = PlayerPrefs.GetInt("highScore");
        }
        else
        {
            highScore = 0;
        }

        //端末単体で動かすので、次のゲームするときにはguid削除
        PlayerPrefs.DeleteKey("guid");
    }

    IEnumerator GameReady()
    {
        yield return new WaitForSeconds(2.5f);
        ReadyText.SetActive(false);
        GoText.SetActive(true);
        yield return new WaitForSeconds(.5f);
        GoText.SetActive(false);
    }

    //リザルト画面転移
    IEnumerator ResultReady()
    {

        yield return new WaitForSeconds(33);
        TimeUp.SetActive(true);
        //時間よとまれ！
        player.isPlaying = false;
        yield return new WaitForSeconds(2);
        TimeUp.SetActive(false);
        audioScript.StopAudio();
        //スコア、時間表示、ポーズボタンを非表示
        UISetActive(false);
        //リザルト画面をアクティブに
        result.SetActive(true);
        //スコア画面を表示
        ResultScore.text = "Score:" + score.totalScore;
        audioScript.StartAudio(3);
    }

    // Update is called once per frame
    void Update()
    {
        if (player.isPlaying)
        {
            currentTime += Time.deltaTime;
            times = 30 - currentTime;

            //残り時間表示
            GameTime.text = "" + Mathf.FloorToInt(times);
            TimeGauge.fillAmount = times / 30;

            
            //Score表示
            for (int i = 0; i < 4; i++)
            {
                Scoretext[i].text = score.scoreVal[i].ToString();
            }

            Scoretext[4].text = score.totalScore.ToString();
        }
    }

    //リトライボタンが押されたら
    public void OnRetry()
    {
        SceneManager.LoadScene("main");
    }

    //ホームボタンが押されたら
    public void OnHome()
    {
        SceneManager.LoadScene("start");
    }

    //ランキングボタンが押されたら
    public void OnRanking()
    {
        naichilab.RankingLoader.Instance.SendScoreAndShowRanking(score.totalScore);
    }


    //ポーズボタンが押されたら
    public void OnPause()
    {
        Debug.Log("pause");
        if (Time.timeScale == 1.0f)
        {
            Time.timeScale = 0.0f;
            pause.SetActive(true);
            UISetActive(false);
        }
        else
        {
            Time.timeScale = 1.0f;
            pause.SetActive(false);
            UISetActive(true);
        }
    }

    void UISetActive(bool IO)
    {
        if(IO){
            gameUI.SetActive(true);
        }
        else
        {
            gameUI.SetActive(false);
        }
    }
}
