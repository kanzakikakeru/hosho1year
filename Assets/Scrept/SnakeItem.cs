﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnakeItem : MonoBehaviour {

    float CurrentTime;
    Vector3 ObjPos;
    float PosX;

    // Use this for initialization
    void Start () {
	}

    //オブジェクトがインスタンス化されたら
    void OnEnable()
    {
        CurrentTime = Time.time;
        PosX = transform.position.x;
    }

    // Update is called once per frame
    void Update () {

        ObjPos = transform.position;
        transform.position = new Vector3(Sin(PosX), ObjPos.y, ObjPos.z);

        //地面より下に行ったら消去
        if (gameObject.transform.position.y <= -1)
        {
            Destroy(gameObject);
        }

    }

    float Sin(float pos)
    {
        float sin = Mathf.Sin((Time.time - CurrentTime) * 2) * 2 + pos;
        return sin;
    }

}
