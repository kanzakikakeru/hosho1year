﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Score : MonoBehaviour {

    private int beforeGetItem;
    public int totalScore;
    public int[] scoreVal = new int[4];
    /*
    //  ScoreArreyの内容
    //  
    //  0 ketchup
    //  1 masterd
    //  2 sausage
    //  3 bans
    */

    public Audio audioScript;

    [Header("prefab")]
    [SerializeField]
    private GameObject getItemCanvas;

    // Use this for initialization
    void Start () {
        //変数初期化
        totalScore = 0;
        getItemCanvas = (GameObject)Resources.Load("Prefab/getItemCanvas");
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    

    //アイテムをとったとき
    public void GetItem(int itemVal, Collider collider)
    {
        if (itemVal == 0)
        {
            audioScript.StartAudio(1);
            Destroy(collider.gameObject);
            for (int i = 0; i < 4; i++)
            {
                scoreVal[i] = 0;
            }
        }
        else if (itemVal >= 5)
        {
            audioScript.StartAudio(0);
            Destroy(collider.gameObject);
            beforeGetItem = itemVal;
            scoreVal[itemVal - 5] += 10;
            Instantiate(getItemCanvas, new Vector3(transform.position.x + 2, transform.position.y + 1, transform.position.z), new Quaternion());

        }
        else
        {
            audioScript.StartAudio(0);
            Destroy(collider.gameObject);
            beforeGetItem = itemVal;
            scoreVal[itemVal - 1]++;
            Instantiate(getItemCanvas, new Vector3(transform.position.x + 2, transform.position.y + 1, transform.position.z), new Quaternion());
        }
    }
}
