﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Audio : MonoBehaviour {

    [Header("Audio")]
    [SerializeField]
    private AudioSource Source;
    [SerializeField]
    public AudioClip[] Clip = new AudioClip[4];

    // Use this for initialization
    void Start () {
        Source = gameObject.GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void StartAudio(int val)
    {
        Source.PlayOneShot(Clip[val]);
    }

    public void StopAudio()
    {
        Source.Stop();
    }
}
