﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    private float playerSpeed;
    public bool isPlaying;
    public Score score;

	// Use this for initialization
	void Start () {
        isPlaying = false;
        playerSpeed = 12.0f;
        StartCoroutine(GameStart());
    }

    IEnumerator GameStart()
    {
        yield return new WaitForSeconds(3);
        isPlaying = true;
    }
	
	// Update is called once per frame
	void Update () {
        if (isPlaying)
        {
            
            //PC用移動操作
            if (Input.GetKey(KeyCode.A))
            {
                transform.position += Vector3.left * playerSpeed * Time.deltaTime;
            }
            if (Input.GetKey(KeyCode.D))
            {
                transform.position += Vector3.right * playerSpeed * Time.deltaTime;
            }

            //スマホ用移動操作(仮実装)
            if (Input.touchCount > 0)
            {
                if (Input.GetTouch(0).position.x < Screen.width / 2)
                {
                    transform.position += Vector3.left * playerSpeed * Time.deltaTime;
                }
                else if (Input.GetTouch(0).position.x > Screen.width / 2)
                {
                    transform.position += Vector3.right * playerSpeed * Time.deltaTime;
                }

            }
        }
        //移動制限
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, -9, 9), 1, 3);
	}

    private void OnTriggerEnter(Collider collider)
    {
        switch (collider.tag)
        {
            case "garbage":
                score.GetItem(0, collider);
                break;

            case "ketchup":
                score.GetItem(1, collider);
                break;

            case "masterd":
                score.GetItem(2, collider);
                break;

            case "sausage":
                score.GetItem(3, collider);
                break;

            case "bans":
                score.GetItem(4, collider);
                break;

            case "10ketchup":
                score.GetItem(5, collider);
                break;

            case "10masterd":
                score.GetItem(6, collider);
                break;

            case "10sausage":
                score.GetItem(7, collider);
                break;

            case "10bans":
                score.GetItem(8, collider);
                break;

        }

        int min = 100;
        for (int i = 0; i <= 3; i++)
        {
            if (min > score.scoreVal[i])
            {
                min = score.scoreVal[i];

            }
        }
        score.totalScore += min;
        for (int i = 0; i <= 3; i++)
        {
            score.scoreVal[i] -= min;
        }

    }
}
