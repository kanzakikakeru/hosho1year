﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class getPointText : MonoBehaviour {

    GameObject Player;
    public bool isGetItem = true;
    float CurrentTime;
    float AnimValue;
    public Animation Anim;

    // Use this for initialization
    void Start () {
        Player = GameObject.FindWithTag("Player");
        Anim = gameObject.GetComponent<Animation>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        transform.position += Vector3.up * 0.5f * Time.deltaTime;
        if(isGetItem == true)
        {
            Anim.Play();
            isGetItem = false;
        }

        if (!GetComponent<Animation>().isPlaying)
        {
            Destroy(gameObject);
        }
    }

    private void OnEnable()
    {
        //gameObject.transform.position = new Vector3(Player.transform.position.x + 2, 2.0f, 3.0f);
        isGetItem = true;
    }
}
