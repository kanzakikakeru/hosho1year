﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mobSpawn : MonoBehaviour {

    //変数宣言
    //落下アイテム
    GameObject[] enemy = new GameObject[7];
    public Player player;

    float currentTime;
    

	// Use this for initialization
	void Start () {
        enemy[0] = (GameObject)Resources.Load("Prefab/mob1");
        enemy[1] = (GameObject)Resources.Load("Prefab/mob2");
        enemy[2] = (GameObject)Resources.Load("Prefab/mob3");
        enemy[3] = (GameObject)Resources.Load("Prefab/mob4");
        enemy[4] = (GameObject)Resources.Load("Prefab/SpeedUpMob");
        enemy[5] = (GameObject)Resources.Load("Prefab/snakeMob");
        enemy[6] = (GameObject)Resources.Load("Prefab/garbageMob");
    }
	
	// Update is called once per frame
	void Update () {
        if (player.isPlaying)
        {
            currentTime += Time.deltaTime;
            if (2 < currentTime)
            {
                currentTime = 0;
                int random = Random.Range(0, 10);
                if (random >= 7) random -= 7;
                float spawnX = -11;
                if (Random.Range(0, 2) == 1)
                {
                    spawnX = 11;
                }
                Instantiate(enemy[random], new Vector3(spawnX, gameObject.transform.position.y, 5), new Quaternion());
            }
        }
	}

}
